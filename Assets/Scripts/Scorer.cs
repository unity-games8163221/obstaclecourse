using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scorer : MonoBehaviour
{

    int hits = 0;

    private void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.tag != "hit"){
            hits = hits + 1;
            Debug.Log("Bump Score: " + hits);
        }
        else {
            Debug.Log("Already Bumped");
        }

    }
}
