using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dropper : MonoBehaviour
{
    [SerializeField] private float fireTime = 3.0f;
    MeshRenderer meshRend;
    Rigidbody rigidBody;

    void Start()
    {
        meshRend = GetComponent<MeshRenderer>();
        rigidBody = GetComponent<Rigidbody>();

        meshRend.enabled = false;
        rigidBody.useGravity = false;
    }

    // Update is called once per frame
    void Update()
    {

        if (Time.time > fireTime) {
            meshRend.enabled = true;
            rigidBody.useGravity = true;
        }
    }
}
